package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type PingResponse struct {
	Result string `json:"result"`
}

func ping(w http.ResponseWriter, r *http.Request) {
	pong, _ := json.Marshal(PingResponse{Result: "pong"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(pong)
}

func main() {
	log.Println("Application started")

	mux := http.NewServeMux()
	mux.HandleFunc("/", ping)

	http.ListenAndServe("localhost:8000", mux)
}
